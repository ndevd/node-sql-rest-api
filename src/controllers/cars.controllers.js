import { pool } from '../db.js'

export const getCars = async (req, res) => { //async and await, asynchronous
    try{
        const [rows] = await pool.query('SELECT * FROM car')
        res.json(rows)

    }catch (error){
        return res.status(500).json({
            message: 'Something was wrong'
        })
    }
}

export const getCar = async (req, res) => {
    try{
        const [rows] = await pool.query('SELECT * FROM car WHERE id = ?', [req.params.id])

        if (rows.length <= 0) return res.status(404).json({
            message : 'Car not found'
        })
        
        res.json(rows[0])

    }catch (error){
        return res.status(500).json({
            message: 'Something was wrong'
        })
    }
}


export const createCar = async (req, res) => {
    const {brand, model, color, year} = req.body

    try{
        const [rows] = await pool.query('INSERT INTO car (brand, model, color, year) VALUES (?, ?, ?, ?)', [brand, model, color, year])
       
        res.send({ 
            id: rows.insertId,
            band,
            model,
            color,
            year 
            })

    }catch (error){
         return res.status(500).json({
            message: 'Something was wrong'
        })
    }
}

export const deleteCar  = async (req, res) => {
    try{
        const [result] = await pool.query('DELETE FROM car WHERE id = ?', [req.params.id])

        if(result.affectedRows <= 0) return res.status(404).json({
            message: 'Car not found'    
        }) 

        res.sendStatus(204) //means that everything went well but nothing is answered to the client

    }catch (error){
        return res.status(500).json({
            message: 'Something was wrong'
        })
    }
}

export const updateCar  = async (req, res) => {
    const {id} = req.params
    const {brand, model, color, year} = req.body;

    try{
        const [result] = await pool.query('UPDATE employee SET brand = IFNULL(?, brand), model = IFNULL(?, model), color = IFNULL(?, color), year = IFNULL(?, year) WHERE id = ?', [brand, model, color, year])
        console.log(result)

        //If the affected rows equals zero, it means I don't update anything
        if(result.affectedRows === 0) return res.status(404).json({
            message: 'Car not found'
        })

        const [rows] = await pool.query('SELECT * FROM car WHERE id = ?', [id])

        res.json(rows[0])

    }catch (error){
        return res.status(500).json({
            message: 'Something was wrong'
        })
    }
}