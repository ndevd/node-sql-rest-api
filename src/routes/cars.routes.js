import { Router } from "express" //Router Module

import { getCars, createCar, updateCar, deleteCar, getCar } from "../controllers/cars.controller.js"
const router = Router()

//routes or endpoints that the client application will request
//get post put delete methods
router.get('/cars', getCars)

router.get('/cars/:id', getCar)

router.post('/cars', createCar) 

router.put('/cars/:id', updateCar)

router.delete('/cars/:id', deleteCar)

export default router