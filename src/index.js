import express from 'express' //framework express

import carsRoutes from './routes/cars.routes.js' //default export
                                                     //you can give it any name
import indexRoutes from './routes/index.routes.js'

import {PORT} from './config.js'

const app = express()
app.use(express.json()) //Method to get the data and convert it into json
                        //then gives it to the routes (below)

app.use(indexRoutes)
app.use('/api', carsRoutes) //use cars routes


//servidor
app.listen(PORT) // listening port 3000
console.log('Server running on port', PORT)