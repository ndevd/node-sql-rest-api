import {config} from 'dotenv'

config()

//it is now possible to read environment variables

export const PORT = process.env.PORT || 3000
export const DB_USER = process.env.DB_USER || 'root' //put username here
export const DB_PASSWORD = process.env.DB_PASSWORD || '' ////put password here
export const DB_HOST = process.env.DB_HOST || 'localhost'
export const DB_DATABASE = process.env.DB_DATABASE || 'cars'
export const DB_PORT = process.env.DB_PORT || 3306 